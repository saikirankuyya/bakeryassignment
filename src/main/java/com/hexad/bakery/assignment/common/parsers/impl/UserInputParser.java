package com.hexad.bakery.assignment.common.parsers.impl;

import java.util.List;
import java.util.Map;

import com.hexad.bakery.assignment.common.exceptions.InputException;
import com.hexad.bakery.assignment.common.parsers.Parser;

import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.toMap;
import static com.hexad.bakery.assignment.common.Constants.INVALID_USER_INPUT;

public class UserInputParser implements Parser<Integer> {

    @Override
    public Map<String, Integer> parseList(List<String> lines) {
        try {
            return lines.stream()
                    .map(line -> line.split(USER_INPUT_SEPARATOR))
                    .collect(toMap(o -> o[1].trim(), o -> parseInt(o[0].trim())));
        } catch (Exception ex) {
            throw new InputException(INVALID_USER_INPUT);
        }
    }
}

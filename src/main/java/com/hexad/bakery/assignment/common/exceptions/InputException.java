package com.hexad.bakery.assignment.common.exceptions;

public class InputException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InputException(String msg) {
		super(msg);
	}
}

package com.hexad.bakery.assignment.common.exceptions;

public class ReaderException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReaderException(String msg) {
        super(msg);
    }
}

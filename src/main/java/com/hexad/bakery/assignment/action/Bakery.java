package com.hexad.bakery.assignment.action;

import com.hexad.bakery.assignment.io.Reader;
import com.hexad.bakery.assignment.io.impl.UserInputReader;
import static com.hexad.bakery.assignment.common.Constants.ORDER_TEXT;
import static com.hexad.bakery.assignment.common.Constants.LINE;
import static com.hexad.bakery.assignment.io.impl.ConsoleWriter.write;

public class Bakery {
	private Reader reader = UserInputReader.getInstance();
	private OrderProcessor orderProcessor;
	private boolean open;

	public void open() {
		this.open = true;
		orderProcessor = new OrderProcessor(this);

		while (open) {
			write(ORDER_TEXT);
			write(orderProcessor.process(reader.readValue()));
			write(LINE);
		}
	}

	public void close() {
		this.open = false;
	}
}

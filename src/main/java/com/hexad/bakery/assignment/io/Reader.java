package com.hexad.bakery.assignment.io;

@FunctionalInterface
public interface Reader {
    String readValue();
}

package com.hexad.bakery.assignment.io.impl;

import java.util.Scanner;

import com.hexad.bakery.assignment.io.Reader;

import static java.util.Objects.isNull;

public class UserInputReader implements com.hexad.bakery.assignment.io.Reader {
    private static Reader reader;
    private static Scanner scanner;

    private UserInputReader() {
        scanner = new Scanner(System.in);
    }

    public static Reader getInstance() {
        if(isNull(reader)) {
            reader = new UserInputReader();
        }

        return reader;
    }

    @Override
    public String readValue() {
        return scanner.nextLine();
    }
}
